# Forkio

---

## Технологии изпользованые в: `forkio`

- Адаптивная верстка на `html` и `css` с использованием `javascript`;

- Технологии `css grid` и `css flex`;

- Менеджер пакетов `npm`;

- Менеджер для автоматизации рутинных задач при веб-разработке `Gulp`;

- Препроцессор css - `scss`;

---

## Роботали над проектом:

1. **Иван Куличев**(группа fe-online);

   - Выполнил секцию `Revolutionary Editor`, `Here is what you get`;

   - А также секцию `Fork Subscription Pricing`;

2. **Халифа Салах**(группа fe-online);

   - Выполнил всю `Шапку сайта` и секцию `People Are Talking About Fork`;

   - Также написал весь `js file`;
